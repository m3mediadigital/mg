<x-layout>
    <x-slot name="content">
        <vue-header csrf="{{ csrf_token() }}"></vue-header>
        <vue-slide csrf="{{ csrf_token() }}"></vue-slide>
        <vue-nio-card csrf="{{ csrf_token() }}"></vue-nio-card>
        <vue-differentials csrf="{{ csrf_token() }}"></vue-differentials>
        <vue-machine csrf="{{ csrf_token() }}"></vue-machine>
        <vue-withdraw csrf="{{ csrf_token() }}"></vue-withdraw>
        <vue-how-it-works csrf="{{ csrf_token() }}"></vue-how-it-works>
        <vue-information csrf="{{ csrf_token() }}"></vue-information>
        <vue-credit csrf="{{ csrf_token() }}"></vue-credit>
        <vue-tax csrf="{{ csrf_token() }}"></vue-tax>
        <vue-cp-resolve csrf="{{ csrf_token() }}"></vue-cp-resolve>
        <vue-about csrf="{{ csrf_token() }}"></vue-about>
        <vue-mg csrf="{{ csrf_token() }}"></vue-mg>
        {{-- <vue-hr csrf="{{ csrf_token() }}"></vue-hr> --}}
        <vue-footer csrf="{{ csrf_token() }}"></vue-footer>
    </x-slot>
</x-layout>

