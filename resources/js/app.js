/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('vue-slide', require('./components/Slide.vue').default);
Vue.component('vue-header', require('./components/Header.vue').default);
Vue.component('vue-we-are', require('./components/WeAre.vue').default);
Vue.component('vue-nio-card', require('./components/NioCard.vue').default);
Vue.component('vue-differentials', require('./components/Differentials.vue').default);
Vue.component('vue-machine', require('./components/Machine.vue').default);
Vue.component('vue-withdraw', require('./components/Withdraw.vue').default);
Vue.component('vue-how-it-works', require('./components/HowItWorks.vue').default);
Vue.component('vue-credit', require('./components/Credit.vue').default);
Vue.component('vue-information', require('./components/Information.vue').default);
Vue.component('vue-tax', require('./components/Tax.vue').default);
Vue.component('vue-cp-resolve', require('./components/CPResolve.vue').default);
Vue.component('vue-about', require('./components/About.vue').default);
Vue.component('vue-mg', require('./components/MG.vue').default);
Vue.component('vue-hr', require('./components/HR.vue').default);
Vue.component('vue-footer', require('./components/Footer.vue').default);
Vue.component('vue-call-to-action',require('./components/CallToAction.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
